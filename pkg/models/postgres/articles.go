package postgres

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"software.net/fanjournal/pkg/models"
	"time"
)

const (
	insertSql                 = "INSERT INTO articles (title, content, date, author, category, image) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	getArticleById            = "SELECT id, title, content, date, author, category, image FROM articles WHERE id=$1"
	getLastTenCreatedArticles = "SELECT id, image, author,title, date, category FROM articles LIMIT 10"
	getArticleByCategory      = "SELECT id, image, author,title, date, category FROM articles WHERE category = $1"
	addLike                   = ""
)

type ArticleModel struct {
	Pool *pgxpool.Pool
}

func (m *ArticleModel) Filter(category string) ([]*models.Article, error) {
	articles := []*models.Article{}
	rows, err := m.Pool.Query(context.Background(), getArticleByCategory, category)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		a := &models.Article{}
		err = rows.Scan(&a.Id, &a.Image, &a.Author, &a.Title, &a.Date, &a.Category)
		if err != nil {
			return nil, err
		}
		articles = append(articles, a)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return articles, nil
}

func (m *ArticleModel) Insert(title, content, category, image string) (int, error) {
	var id uint64
	err := m.Pool.QueryRow(context.Background(), insertSql, title, content, time.Now(), "SiteUser", category, image).Scan(&id)
	if err != nil {
		return 0, err
	}
	return int(id), nil
}

func (m *ArticleModel) Get(id int) (*models.Article, error) {
	a := &models.Article{}
	err := m.Pool.QueryRow(context.Background(), getArticleById, id).Scan(&a.Id, &a.Title, &a.Content, &a.Date, &a.Author, &a.Category, &a.Image)
	if err != nil {
		if err.Error() == "No rows in result set" {
			return nil, models.ErrNoRecord
		} else {
			return nil, err
		}
	}
	return a, nil
}

func (m *ArticleModel) Latest() ([]*models.Article, error) {
	articles := []*models.Article{}
	rows, err := m.Pool.Query(context.Background(), getLastTenCreatedArticles)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		a := &models.Article{}
		err = rows.Scan(&a.Id, &a.Image, &a.Author, &a.Title, &a.Date, &a.Category)
		if err != nil {
			return nil, err
		}
		articles = append(articles, a)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return articles, nil
}
