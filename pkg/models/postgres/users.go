package postgres

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"golang.org/x/crypto/bcrypt"
	"software.net/fanjournal/pkg/models"
	"time"
)

const (
	insert = "INSERT INTO users (name, email, hashed_password, created) VALUES ($1, $2, $3, $4) RETURNING id"
	auth   = "SELECT id, hashed_password FROM users WHERE email = $1 AND active = TRUE"
)

type UserModel struct {
	Pool *pgxpool.Pool
}

func (m *UserModel) Insert(name, email, password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 12)
	if err != nil {
		return err
	}
	var id uint64
	err = m.Pool.QueryRow(context.Background(), insert, name, email, hashedPassword, time.Now()).Scan(&id)
	if err != nil {
		if errors.Is(err, models.ErrDuplicateEmail) {
			return models.ErrDuplicateEmail
		}
		return models.ErrDuplicateEmail
	}
	return nil
}

func (m *UserModel) Authenticate(email, password string) (int, error) {
	var id int
	var hashedPassword []byte

	err := m.Pool.QueryRow(context.Background(), auth, email).Scan(&id, &hashedPassword)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, models.ErrInvalidCredentials
		}
	}

	err = bcrypt.CompareHashAndPassword(hashedPassword, []byte(password))
	if err != nil {
		if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
			return 0, models.ErrInvalidCredentials
		} else {
			return 0, err
		}
	}

	return id, nil
}

func (m *UserModel) Get(id int) (*models.User, error) {
	return nil, nil
}
